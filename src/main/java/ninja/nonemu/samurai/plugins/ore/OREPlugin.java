package ninja.nonemu.samurai.plugins.ore;

import java.io.IOException;
import ninja.nonemu.irc.ChatSender;
import ninja.nonemu.irc.Color;
import ninja.nonemu.samurai.command.CommandInfo;
import ninja.nonemu.samurai.command.CommandSender;
import ninja.nonemu.samurai.connection.Channel;
import ninja.nonemu.samurai.connection.ChatEvent;
import ninja.nonemu.samurai.connection.Connection;
import ninja.nonemu.samurai.connection.User;
import ninja.nonemu.samurai.connection.UserJoinEvent;
import ninja.nonemu.samurai.connection.UserLeaveEvent;
import ninja.nonemu.samurai.event.EventHandler;
import ninja.nonemu.samurai.event.Priority;
import ninja.nonemu.samurai.plugin.PluginBase;

public class OREPlugin extends PluginBase {
    private String[] services;
    private Connection connection;

    public OREPlugin() {
        super("ORECompat");
    }

    @Override
    public void init() throws IOException {
        try {
            loadProperties();
        } catch (IOException e) {
            getLogger().error("Cannot load properties, trying default ones.", e);
            loadDefaultProperties();
            saveProperties();
        }

        services = getProperty("services").split(",");
        connection = getBot().getConnectionManager().getConnection(getProperty("connection"));

        getBot().getEventSystem().registerHandlers(this);

        registerCommand(new CommandInfo(
                "oremsg",
                "Sends a message to a user on one of the ORE services.",
                "oremsg <name> <message>",
                true
        ));
    }

    @Override
    public void cleanup() {

    }

    @Override
    public boolean onCommand(CommandSender sender, CommandInfo info, String label, String[] args) throws Exception {
        if (info.getName().equalsIgnoreCase("oremsg") && args.length >= 2) {
            String[] words = new String[args.length - 1];
            System.arraycopy(args, 1, words, 0, words.length);

            String msg = String.join(" ", (CharSequence[]) words);

            for (String service : services) {
                connection.sendChat(service, "/msg " + args[0] + " " + msg);
            }
            return true;
        }
        return false;
    }

    @EventHandler(priority = Priority.HIGHEST, acceptSubclass = false)
    public void onChatEvent(ChatEvent event) {
        ChatSender sender = event.getSender();
        String stripText = Color.stripColors(event.getText());

        if (isService(sender)) {
            String[] words = event.getText().split(" ");

            if (words.length > 0 && words[0].endsWith(":")) {

                String name = words[0].substring(0, words[0].length() - 1);
                String[] textWords = new String[words.length - 1];
                System.arraycopy(words, 1, textWords, 0, textWords.length);

                event.cancel();
                getBot().getEventSystem().dispatchEvent(new ChatEvent(
                        new OREUser(getBot(), Color.stripColors(name), (User) sender),
                        event.getTarget(),
                        String.join(" ", (CharSequence[]) textWords)
                ));
            } else if (stripText.contains("joined the game")) {
                event.cancel();
                getBot().getEventSystem().dispatchEvent(new UserJoinEvent(
                        new OREUser(getBot(), Color.stripColors(words[0]), (User) sender),
                        (Channel) event.getTarget()
                ));
            } else if (stripText.contains("left the game")) {
                event.cancel();
                getBot().getEventSystem().dispatchEvent(new UserLeaveEvent(
                        new OREUser(getBot(), Color.stripColors(words[0]), (User) sender),
                        (Channel) event.getTarget(),
                        "No comment"
                ));
            }
        }
    }

    public boolean isService(ChatSender sender) {
        return sender instanceof User && isService(sender.getName());
    }

    public boolean isService(String name) {
        for (String service : services) {
            if (name.equalsIgnoreCase(service)) {
                return true;
            }
        }
        return false;
    }
}

package ninja.nonemu.samurai.plugins.ore;

import ninja.nonemu.samurai.Bot;
import ninja.nonemu.samurai.connection.Connection;
import ninja.nonemu.samurai.connection.User;
import ninja.nonemu.samurai.connection.UserMask;

public class OREUser implements User {

    private final Bot bot;
    private final UserMask mask;
    private final User service;

    public OREUser(Bot bot, String name, User service) {
        this.bot = bot;
        mask = new UserMask(name, "ore", service.getHostname());
        this.service = service;
    }

    @Override
    public String getName() {
        return getNickname();
    }

    @Override
    public UserMask getMask() {
        return null;
    }

    @Override
    public String getNickname() {
        return mask.getNickname();
    }

    @Override
    public String getUsername() {
        return mask.getUsername();
    }

    @Override
    public String getHostname() {
        return mask.getHostname();
    }

    @Override
    public Connection getConnection() {
        return service.getConnection();
    }

    @Override
    public void sendChat(String text) {
        service.sendChat("/msg " + getName() + " " + text);
    }

    @Override
    public boolean isOperator() {
        return bot.getCommandSystem().isOperator(mask);
    }

    @Override
    public boolean isWhitelisted() {
        return bot.getCommandSystem().isWhitelisted(mask);
    }

    @Override
    public boolean isBlacklisted() {
        return bot.getCommandSystem().isBlacklisted(mask);
    }
}